# 测试相关

因为有30个专项，所以自动化测试非常重要。

```python
class Assign:
    def __init__(self, model, distris, vecs):
        '''
        传入指派模型，学科分布列表，指南向量列表（用小节划分）
        '''
        
    def appr_assign(self, num):
        '''
        因为各小节选的专家会有重叠，所以去重后的人数会比预期的数要少，所以这个只是一个粗略分配的函数。
        '''
        
    def accu_assign(self, N, L = 1, R = 5000):
        '''
        使用二分法得到相对准确的指派，一般人数比预期只有1人左右的浮动。L和R是搜索的左右边界。
        Warning: 这个函数非常耗时。
        '''
        
    def accu_assign_test(self, N, test, L = 1, R = 5000):
        '''
        测试使用，过滤学科之前的人数是N，返回命中人数和选择人数。
        '''
        
    def accu_assign_filt(self, N, test, L = 1, R = 5000):
        '''
        测试使用，过滤掉命中率低于10%的学科。过滤学科之后的人数是N，返回过滤学科之后的命中人数和选择人数。
        '''

class Test:
    def __init__(self, path):
        '''
        传入人工推荐名单路径
        '''

    def hits(self, se_list, xkys):
        '''
        给定算法推荐名单，以及每个专家对应的学科
        返回命中人数、总人数，过滤学科以后的命中人数、总人数
        '''
```