# 学科分析

学科分析的基础是关键词-学科矩阵，实际上是关键词和学科的联合分布矩阵，目前使用的矩阵是在v2.0系统的基础上，用[科学基金共享资源服务网](http://npd.nsfc.gov.cn/projectSearch.action)的数据进行了补充。

在分析的算法上，使用“前向限制，后向过滤”的方法，所谓“前向限制”，是发现一级学科的预测要比二级学科的预测准确，二级要比三级准确，因此下一级学科的预测要在上一级的学科中选择；所谓“后向过滤”，是指最终的一二三级主要学科，取决于三级学科，从三级学科向上走，没有涉及到的上级学科就会被过滤掉。

学科分析模块的功能就是用一个关键词-学科矩阵初始化，给定文本，给出学科分布。另外，也有分析主要学科的API。

此外，关于学科分析也设置了相应的人工干预措施，比如规避某些学科。

```python
class SubjDistri:
    def __init__(self, distri_path, min_len = Config.MIN_WORD_LEN_SUBJ):
        '''
        给定关键词-学科矩阵的文件路径初始化
        关键词-学科矩阵是一个pickle字典，key是关键词，value是学科分布字典
        第二个参数主要是因为学科分析需要依赖于分词模块
        '''
                
    def analyse(self, text):
        '''
        给定文本字符串，返回学科分布字典，key是学科，value是比重
        '''

    def mainsubj(self, subj_distribution, thresh_proportion = 0.7, min_proportion = 0.1, dec_threshold = 10):
        '''
        给定学科分布字典，返回主要学科字典
        有三个阈值
        1. thresh_proportion: 已经选择的学科分布占比超过这个值就不再往后选
        2. min_proportion: 当前学科占比已经不足这个值就不再往后选
        3. dec_threshold: 当前学科比前一名下降比例超过这个值就不再往后选
        '''
```