# 词网训练

词网训练在这里用到的是单语模型，双语Joint Embedding训练的代码在另一个代码库里。因为后面求向量加权平均的时候需要TF-IDF，所以在训练模型的时候会顺便统计每个词的DF。

```python
class TrainWV:
    '''
    给定语料，确定输出路径，训练word2vec模型，并且得到每个词的DF
    为了减少内存占用，并不是把语料读入内存再训练，而是边遍历文件边训练
    '''
    def __init__(self, fns, outfn):
        '''
        fns是语料的文件名列表，每个文件都是每行一段语料，词和词之间用\t隔开
        outfn是输出文件名
        '''
    def train(self):
        '''
        训练fns，把模型保存到outfn
        '''
```