# 指派模型

目前尝试了三种方法：word2vec向量平均，doc2vec，word2vec向量加权平均，代码里都提供了相应的实现。实际上指派模型可以抽象为更通用的接口，只要能实现这些接口，都能够实现指派功能。上面的三个方法都能归纳为下面这个接口模式：

```python
class Model:
    def __init__(self, dic, asso):
        '''
        传入中文词典以及英译中词典
        '''

    def train(self, sentences, outfn):
        '''
        给定专家文本信息，保存训练好的专家模型到outfn，该模型只要能够支持infer和most_similar操作即可，不一定非要用Doc2Vec自带的infer和most_similar，也可以自定义，可以参照WordModel代码
        '''
        
    def load(self, model_path):
        '''
        加载训练好的模型
        '''
    
    def infer(self, text):
        '''
        给定文本，返回向量表示
        '''
    
    def most_similar(self, vec):
        '''
        给一个向量，返回专家跟该向量的相似度排名信息
        '''
```