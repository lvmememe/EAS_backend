# 科技部评审专家智能指派v3.0代码文档

* [概述](README.md)

### 代码

* [文本分词](code/text-slicer.md)
* [学科分析](code/subject.md)
* [词网训练](code/train.md)
* [指南相关](code/guideline.md)
* [指派模型](code/expert.md)
* [测试相关](code/test.md)

### 数据

* [关键词](data.md#关键词)
* [专家](data.md#专家)
* [学科](data.md#学科)

