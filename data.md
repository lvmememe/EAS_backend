# 数据

### 关键词

包括word2vec训练好的keyword模型，中文词典，英译中词典，停用词。

### 专家

为了减少数据库的读取操作，直接使用了从数据库里导出的专家信息缓存。从mongo数据库导出的原始文件是expert.json，经过extract_expert_info.py这个文件处理，可以得到expert.pkl，也就是项目代码用到的专家信息。

### 学科

学科矩阵有三个，分别是一、二、三级学科。如果方法没变的话，对于学科分析的优化只需要完善这个矩阵就可以了。